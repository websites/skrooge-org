# Skrooge.org Website

This is the git repository for [skrooge.org](https://skrooge.org), the website for Skrooge, a personal finances manager, powered by KDE.

As a (Hu)Go module, it requires both [Hugo](https://gohugo.io/) and Go to work.

## Development
Read [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/).

## I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n).
