---
date: 2025-01-11
title: Skrooge 25.1.0 released
layout: annc
---

{{< i18n "news.intro" "25.1.0" >}}

<!--more-->

This is the first version for Kf6/Qt6.

### Changelog

* Correction bug 494197: Shortcut for Setting Status to Checked
* Correction bug 494159: Wrong decimal separator in CSV import
* Correction bug 494516: Categories "closed" are not displayed
* Correction bug 494023: Downloading values from yahoo fails HTTP Error 401: Unauthorized
* Correction bug 494077: document History panel and better document viewing transactions modified by an action
* Correction bug 498157: Inconsistent icons in the Pages sidebar
* Correction: Replace yahoo source (not working) by boursorama source
* Correction: More robust copy of tables
* Migration: Support build on qt6/kf6
* Correction: Fix performances issue on qt6 due to QDateTime::fromString
