---
date: 2023-07-02
title: Skrooge 2.30.0 released
layout: annc
---
{{< i18n "news.intro" "2.30.0" >}}

### Changelog

* Correction bug 435762: Income&Expenditure widget don't match with Income&Expense report
* Correction bug 454965: Stats plugin should use XDG_STATE_HOME rather than the hardcoded .skrooge path to store files
* Correction bug 467153: Skrooge crash opening dashboard, after QML dashboard only introduced
* Correction bug 467599: Issue with reconciliation of portfolio accounts
* Correction bug 468329: Import via woob 3.5
* Correction: Remove all dashboard to keep only the qml version
