---
date: 2023-01-22
title: New web site
---
We are excited to announce the launch of our new website, now built with the popular static site generator, [Hugo](https://gohugo.io). This change marks a significant shift away from our previous CMS, Drupal, and brings a fresh new look that aligns with the style of KDE, our parent organization.

We understand that change can be difficult, but we believe that this move will greatly benefit our users. The switch to [Hugo](https://gohugo.io) allows for faster loading times, improved security, and a more streamlined user experience. Additionally, the new design is fully responsive, ensuring that the site looks great on any device.

One of the most exciting features of the new website is that it will be **translated into multiple languages**, making it more accessible to our global audience. This will ensure that users can access the same information, no matter what language they speak.

We hope that you enjoy the new website and would love to hear your feedback. Don't hesitate to reach out to us with any suggestions or questions. 

Thank you for your continued support of Skrooge and the KDE community.

