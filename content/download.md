---
title: Download
name: Skrooge
scssFiles:
- /scss/download.scss
layout: download
menu:
  main:
    weight: 30
---

{{< get_it type="linux" >}}

## Linux, BSD

**The recommended way is to install Skrooge using your distribution's package management tool.**

{{< store_badge type="appstream" link="appstream://org.kde.skrooge" divClass="d-flex flex-column mb-4" aClass="d-flex" >}}

However, if Skrooge is not yet available in repositories, you should of course bug the distro's packagers to include it, but in the mean time you can [compile and install Skrooge yourself](https://techbase.kde.org/Projects/Skrooge "https://techbase.kde.org/Projects/Skrooge"), it is not that difficult.

{{< /get_it >}}

{{< get_it type="ubuntu" >}}

## Ubuntu

Ubuntu's policy is not to propose the latest software versions, but rather to focus on stability. For that reason, it is possible that the Skrooge version available in official repositories is usually several versions behind the latest stable from developers.

To install Skrooge on Ubuntu from our [PPA](https://launchpad.net/~s-mankowski), you need to:

### Kf6/Qt6 version:

* Open a terminal and enter:

**Stable**
```bash
sudo add-apt-repository ppa:s-mankowski/ppa-kf6
```

**Beta**
```bash
sudo add-apt-repository ppa:s-mankowski/beta-kf6
```

* Tell Ubuntu to re-load the details of each software archive it knows about:

```bash
sudo apt-get update
```

* Install Skrooge:

```bash
sudo apt-get install skrooge-kf6
```

### Kf5/Qt5 version:

* Open a terminal and enter:

**Stable**
```bash
sudo add-apt-repository ppa:s-mankowski/ppa-kf5
```

**Beta**
```bash
sudo add-apt-repository ppa:s-mankowski/beta-kf5
```

* Tell Ubuntu to re-load the details of each software archive it knows about:

```bash
sudo apt-get update
```

* Install Skrooge:

```bash
sudo apt-get install skrooge-kf5
```

{{< /get_it >}}

{{< get_it type="opensuse" >}}

## openSUSE

Download package from [here](https://software.opensuse.org/download.html?project=home%3Amiraks&package=skrooge)

{{< /get_it >}}

{{< get_it type="snap" >}}

## Snap

{{< store_badge type="snapstore" link="https://snapcraft.io/skrooge" divClass="mb-3" >}}

{{< /get_it >}}

{{< get_it type="appimage" >}}

## Appimage

{{< i18n "download.appimage.link" "skrooge-2.30.0-x86\_64.AppImage" "https://download.kde.org/stable/skrooge/skrooge-2.30.0-x86_64.AppImage.mirrorlist" >}}

{{< /get_it >}}

{{< get_it type="flatpak" >}}

## Flatpak

**You can install the stable version from Flathub**

{{< store_badge type="flathub" link="https://flathub.org/apps/details/org.kde.skrooge" divClass="d-flex mb-3" >}}

**You can install the nightly build version too**:

```bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --if-not-exists kdeapps --from https://distribute.kde.org/kdeapps.flatpakrepo
flatpak install kdeapps org.kde.skrooge
```

**To run it:**

```bash
flatpak run org.kde.skrooge
```

{{< /get_it >}}

{{< get_it type="windows" >}}

## Windows

Please read the [Windows Installation](../windows_installation) page.

{{< /get_it >}}

{{< get_it type="source" >}}

## Source Code of Skrooge based on Kf5/Qt5 and Kf6/Qt6

### Latest Stable Version

This is the recommended version for most users. It should be exempt from major bugs.

10 January 2025: [skrooge-25.1.0.tar.xz](https://download.kde.org/stable/skrooge/skrooge-25.1.0.tar.xz.mirrorlist)

Other [stable versions](https://download.kde.org/stable/skrooge/)

### Development Version

If you like living on the edge, or are in interested in testing new features, you may want to try the development version. Be aware though that it might contains severe bugs. Your data should be safe, however.

10 January 2025: [skrooge-25.1.0.tar.xz](https://download.kde.org/stable/skrooge/skrooge-25.1.0.tar.xz.mirrorlist)

Other [unstable versions](https://download.kde.org/unstable/skrooge/)

{{< /get_it >}}
