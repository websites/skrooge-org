---
title: Features
menu:
  main:
   weight: 20
---
## Classical features

### Like in a web browser

|            |             |
|:----------:|:------------|
| ![](/images/multi_tabs.png) | Several tabs to help you organize your work. |
| ![](/images/back_forward.png) | Navigate like in a browser. | 
| ![](/images/bookmarks_browser.png) | Bookmark your preferred reports, graphs, filters, pages, ... | 



### Infinite categories levels
|            |             |
|:----------:|:------------|
| ![](/images/categories.png) | Categories allow you to organize all of your financial transactions.<br>This makes it easier to see where your money comes from and where it goes. |

### Scheduled operations
|            |             |
|:----------:|:------------|
| This is an easy way to add automatically transactions. | ![](/images/scheduled.png) |

### Multi currencies
|            |             |
|:----------:|:------------|
| ![](/images/units.png) | Manage all our currencies, stocks, assets.<br>You can download stocks and currencies prices from many sources too. |

### Manage payees
|            |             |
|:----------:|:------------|
| This is an easy way to manage who is the payee transactions. | ![](/images/payees.png) |

### Reporting

|            |             |
|:----------:|:------------|
| Build the graph you want to well understand how your spend your money.<br>Have a look to the periodic reports (monthly, annually, ...) to understand the progress.<br>Have a quick look on the dashboard. Skrooge is also able to give you advice based on your behavior. | ![](/images/reports.png) |`

### Budget

|            |             |
|:----------:|:------------|
| Budgeting isn't about restriction.<br>It's about setting and reaching your goals.<br>Skrooge can help you to manage your budgets by putting in places simple rules. | ![](/images/budgets.png) |


### Import your accounts from many sources

Skrooge is able to import transactions from many formats (AFB120, QIF, CSV, MT940, OFX, QFX, ...).

For a richer import, Skrooge is able to import documents from many applications (KMYMONEY, Microsoft Money, GNUCASH, GRISBI, HOMEBANK and MONEY MANAGER EX).

And better, Skrooge is able to import directly transactions from all your banks websites in one click.

| Format   |      Comments      |  Import | Export |
|----------|:------------------:|:-------:|:------:|
| Direct import | The direct import from banks is done with [woob](https://woob.tech/). | ![](/images/ok.png) | |
| AFB120 |  French norm | ![](/images/ok.png) | | |
| [.CSV](https://en.wikipedia.org/wiki/Comma-separated_values) | Not a financial specific file formats, but sometimes used by banks or applications. Skrooge allows very flexible processing of these files | ![](/images/ok.png) | ![](/images/ok.png) |
| .gnc | The [GnuCash](https://www.gnucash.org/) file format. | ![](/images/ok.png) | |
| .gsb | The [Grisbi](https://grisbi.org/) file format. | ![](/images/ok.png) | |
| .iif | The [Intuit Interchange Format](https://quickbooks.intuit.com/learn-support/en-us/help-article/list-management/iif-overview-import-kit-sample-files-headers/L5CZIpJne_US_en_US) file format. | ![](/images/ok.png) | ![](/images/ok.png) |
| .json | The javascript file format. | | ![](/images/ok.png) |
| .kmy | The [KMyMoney](https://kmymoney.org/) file format. | ![](/images/ok.png) | ![](/images/ok.png) |
| .ledger | The [ledger](https://www.ledger-cli.org/) file format. | ![](/images/ok.png) | ![](/images/ok.png) |
| .mny | The Microsoft Money file format. | ![](/images/ok.png) | |
| .mmb | The [Money Manager Ex](https://moneymanagerex.org/) file format. | ![](/images/ok.png) | |
| MT940 |  MT940 is a specific SWIFT message type used by the SWIFT network. | ![](/images/ok.png) | | |
| [.OFX](https://en.wikipedia.org/wiki/Open_Financial_Exchange) / [.QFX](https://en.wikipedia.org/wiki/QFX_%28file_format%29)|  Widely used by banks or applications. Standard specifications make it the more robust format when available | ![](/images/ok.png) | | |
| [.QIF](https://en.wikipedia.org/wiki/QIF) | Widely used text format, but with no standard specification. Has some major drawbacks, like not containing the currency used, nor a defined date format. Skrooge does mostly better in processing this format than other applications, but there might still be some tricks here and there. | ![](/images/ok.png) | ![](/images/ok.png) |
| .skg | The Skrooge file format. | ![](/images/ok.png) | ![](/images/ok.png) |
| .sqcipher | The [sqcipher](https://www.zetetic.net/sqlcipher/) file format. | ![](/images/ok.png) | ![](/images/ok.png) |
| .sqlite | The [sqlite](https://www.sqlite.org) file format. | ![](/images/ok.png) | ![](/images/ok.png) |
| .xhb | The [Homebank](http://homebank.free.fr/) file format. | ![](/images/ok.png) | |
| .xml | The Skrooge XML file format. | ![](/images/ok.png) | ![](/images/ok.png) |
| .xml | The [ISO 20022](https://www.iso20022.org/) XML file format. | ![](/images/ok.png) | |

And if you're not satisfied with Skrooge, this is not a problem. Your data is not trapped, indeed you can export them in many formats.

## Advanced features

### Infinite undo/redo
|            |             |
|:----------:|:------------|
| Skrooge records all your actions.<br>This allows to undo or redo them even after the file was closed ! | ![](/images/history_browser.png) |

### Mass update of operations
|            |             |
|:----------:|:------------|
| ![](/images/before_mass_update.png) | To make your life easier, all actions can be done massively. |

### Automatically process operations based on search conditions
With the "Search and process" page, you can search operation and apply a categorisation or a template.

Really useful after an import.

### Track refund of your expenses
|            |             |
|:----------:|:------------|
| ![](/images/trackers.png) | Skrooge can help you to check that you have received the expected refund (e.g. medical). |

### Dashboard
|            |              |
|:----------:|:------------:|
| ![](/images/dashboard_categories.png)<br>![](/images/dashboard_banks_light.png) | You can design your dashboard by using many different components. |

### Reports
|            |             |
|:----------:|:------------|
| Skrooge can produce some periodic reports.<br>| ![](/images/monthly.png) |

Skrooge can download more reports templates:

* Fichier des Ecritures Comptables
* autoentrepreneur
* freelance
* ...

If you want, you can build your own report template too.

### And many other
* Instant filtering on operations and reports.
* Add all properties you want on all objects (transactions, accounts, categories, ...) and including files (pdf, pictures, ...).
* Interests computation.
* Simulations.
* Export from all tables.
* Export from graphs and reports.
* ...

## Videos

A Skrooge user kindly took some of his time to craft some videos:

* Skrooge tutorial #1 [https://vimeo.com/27796633](https://vimeo.com/27796633)
* Skrooge tutorial #2 [https://vimeo.com/27808880](https://vimeo.com/27808880)
* Skrooge tutorial #3 [https://vimeo.com/27893118](https://vimeo.com/27893118)

He also created a comparison between Skrooge, Gnucash and Homebank:
[https://vimeo.com/27799084](https://vimeo.com/27799084)
