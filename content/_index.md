---
title: Skrooge - A personal finances manager, powered by KDE
scssFiles:
- /scss/home.scss
jsFiles:
- /js/home.js
---

Skrooge allows you to manage your personal finances, powered by [KDE](https://www.kde.org). Thanks to its many [features](/features), it is one of the most powerful way to enter, follow, and analyze your expenses.

Based on its KDE foundations, Skrooge can run on many platforms, including of course Linux, BSD, Solaris, but also on macOS, and possibly on Windows.

Skrooge is part of [KDE](https://apps.kde.org), and is released under the [GPL](https://en.wikipedia.org/wiki/GNU_General_Public_License) V3 licence.

* General user information: [KDE Userbase](https://userbase.kde.org/Skrooge)
* The git repository: [KDE Invent](https://invent.kde.org/office/skrooge)
* Bugs and wishes: [KDE Bugtracker](https://bugs.kde.org/enter_bug.cgi?format=guided&product=skrooge)
* Forum: [KDE Discuss](https://discuss.kde.org/tag/skrooge)
* The dedicated page on [KDE Applications](https://apps.kde.org/skrooge)
* Documentation: in case the Skrooge handbook is not installed on your machine, you may browse it [online](https://docs.kde.org/?application=skrooge).
