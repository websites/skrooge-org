---
menu:
  main:
   weight: 40
title: Help
---
Are you looking for help using Skrooge ? Here is a list of useful locations

****

## Forum
As a standard [KDE](https://kde.org) application, Skrooge questions can be asked on KDE Discuss:

[Take me to the forum!](https://discuss.kde.org/)

****

## Documentation
Skrooge documentation is supposed to be installed along the application when using your distribution's packages. If for some reason it is not there, or outdated, you may read it online:

[Take me to the online doc!](https://docs.kde.org/development/en/extragear-office/skrooge/index.html)

****

## Wiki
Skrooge uses the [KDE Wiki](https://userbase.kde.org). As with any wiki, you are encouraged to contribute :)

[Take me to the Wiki!](https://userbase.kde.org/Skrooge)

****

## FAQ
You may find answers to your questions at our own [FAQ](/faq).
