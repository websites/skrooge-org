---
title: Contact
menu:
  main:
    weight: 60
---

You can contact the Skrooge developer in many ways:

* Mail: You can find the email of the main developer in the "About" of Skrooge.

* Forum: If you prefer to use a forum you can use [KDE Discuss](https://discuss.kde.org/).

* Bugs and Wishes: Bugs and Wishes should be reported to the [KDE bug tracker](https://bugs.kde.org/enter_bug.cgi?format=guided&product=skrooge).
