---
title: Donate
menu:
  main:
    weight: 70
---
The Skrooge team does not currently need financial support, thank you.

## Donate to KDE instead:
However, if you wish to support us, we strongly encourage you to make a donation to the [KDE e.V.](https://ev.kde.org/). We rely on [KDE](https://kde.org) for many things, so if you really want to support us, you should definitely consider helping KDE:

* [Make a donation to KDE](https://kde.org/community/donations/)
* [Become a KDE Supporting Member](https://kde.org/community/donations/)

## Send us a small item:
Another option would be to send the Skrooge dev a small item, typical of your country or area! This is a great way for us to know where our users are from, and to remember them! If you wish to send us something, please [contact us](/contact) so that we can give you postal addresses.
