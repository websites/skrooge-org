---
title: Windows Installation
---
Skrooge can be installed on Windows 10 by following this procedure:

1. On Win10 Creators Update with the Windows Subsystem for Linux (WSL) https://docs.microsoft.com/en-us/windows/wsl/install-win10
2. You can use Ubuntu, then on console:
    ```bash
    sudo add-apt-repository ppa:s-mankowski/ppa-kf5
    sudo apt-get update
    sudo apt-get install skrooge-kf5
    ```
4. Install Xming:
    ```bash
    export DISPLAY=:0
    sudo apt install dbus-x11
    dbus-launch
    ```
6. Enable icons in Skrooge:
    ```bash
    export QT_QPA_PLATFORMTHEME=qt5ct
    qt5ct
    sudo apt-get install plasma-workspace
    sudo apt-get install breeze-icon-theme
    sudo apt-get install oxygen-icon-theme
    sudo apt-get install libkf5iconthemes5
    sudo apt-get install libkf5config-bin
    sudo apt-get install systemsettings
    sudo apt-get install systemsettings5
    systemsettings5
    ```
    And choose "Breeze" icons.
5. Add at the end of `~/.config/kdeglobals`:
    ```text
    [Icons]
    Theme=breeze
    ```
